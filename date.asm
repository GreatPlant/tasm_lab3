;     
.486
.MODEL SMALL
    INCLUDE MACROS.MAC
    INCLUDE CONSTS.INC
    INCLUDE DATE_T.INC
LOCALS
    .data
    MOUNTH_SUM DW 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365
    DAY_IN_YEAR DW 365
    LEAP_PERIOD DB 4 ;จแฎชฎแญ๋ฉ ฏฅเจฎค
    .code
;=====================================================
;      ..
;   DATE
; SI -  
; BX - 
; CF - 
;=====================================================
STR_TO_DATE PROC NEAR
    CMP BYTE PTR [SI][2], '.'
    JNE EXCEPT
    CMP BYTE PTR [SI][5], '.'
    JNE EXCEPT
    GETDATE 0
    INRANGE 1 AX 31
    JNC EXCEPT
    SETFIELD DAY BX, AX
    GETDATE 3
    INRANGE 1 AX 12
    JNC EXCEPT
    SETFIELD MOUNTH BX, AX
    GETDATE 6
    SETFIELD YEAR BX, AX
    STC
    RET
EXCEPT:
    CLC
    RET
STR_TO_DATE ENDP

;=====================================================
;    DATE  
;  ..
; SI -  
; BX - 
; ZF - 
;=====================================================
DATE_TO_STR PROC NEAR
    GETFIELD DAY AX, BX
    SETDATE 0
    MOV BYTE PTR [SI][2], '.'
    GETFIELD MOUNTH AX, BX
    SETDATE 3
    MOV BYTE PTR [SI][5], '.'
    GETFIELD YEAR AX, BX
    SETDATE 6
    MOV BYTE PTR [SI][8], 0
    RET
DATE_TO_STR ENDP

;=====================================================
;      
;    ..
; SI -  
; DI -  
; ZF -   
;=====================================================
FIND_DATE_DIFF proc
    XOR AX, AX  ;ฅงใซ์โ โ
    CMP SI, DI  ;เฎขฅเช  ญ  แฎขฏ คฅญจฅ ค โ
    JE @@EXT

    PUSH DX

    GETFIELD YEAR AX, DI
    GETFIELD YEAR DX, SI
    SUB AX, DX
    MOV BX, AX  ;แฎๅเ ญฅญจฅ เ งญจๆ๋ ข ฃฎค ๅ
    IMUL DAY_IN_YEAR

    GETFIELD MOUNTH DX, DI
    ADD AX, MOUNTH_SUM[EDX*2]
    GETFIELD MOUNTH DX, SI
    SUB AX, MOUNTH_SUM[EDX*2]

    GETFIELD DAY DX, DI
    ADD AX, DX
    GETFIELD DAY DX, SI
    SUB AX, DX

    PUSH AX         ;แฎๅเ ญฅญจฅ เฅงใซ์โ โ  กฅง ขจแฎชฎแญ๋ๅ คญฅฉ
    MOV AX, BX
    IDIV LEAP_PERIOD ;ญ ๅฎฆคฅญจฅ ขจแฎชฎแญ๋ๅ คญฅฉ
    XOR AH, AH      ;ฎ็จแโช  ฎแโ โช 
    POP DX          ;ขฎงขเ ้ฅญจฅ เฅงใซ์โ โ 
    ADD AX, DX      ;คฎก ขซฅญจฅ ขจแฎชฎแญ๋ๅ คญฅฉ

    POP DX
@@EXT:
    RET
FIND_DATE_DIFF endp

PUBLIC STR_TO_DATE, DATE_TO_STR, FIND_DATE_DIFF
END
