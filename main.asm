; *****************************************************
; BASE FILE FOR LAB3
; *****************************************************
    .486
    MODEL SMALL
    STACK 200H

    INCLUDE CONSTS.INC
    INCLUDE DATE_T.INC
    INCLUDE MACROS.MAC

    .DATA
        BUFFER DATE_BUFF <>
        DATE_ARRAY DATE ARRSIZE DUP (<>)
        MSG DB "������ ���� ��� enter ��� ��室� ", 0FFH
        ERR_MSG DB "�� �訡����! ���஡�� ᭮��", 0
        INDEX_INP_MSG DB "������ ������� ����", 0
        INDEX_BUF DB 6, 0, 6 DUP (0)
        OUT_MSG DB "������ � ����: ", 0FFH
        REPEAT_MSG DB "������� ����? (y/n) ", 0FFH
    .CODE
MAIN LABEL NEAR
    MOV AX, @DATA
    MOV DS, AX

    XOR ECX, ECX
    LEA DX, BUFFER
    MOV SI, OFFSET BUFFER.BUF_DATA
LP:
    CMP ECX, ARRSIZE
    JGE CALC
    TRY:
        PUTL MSG
        CALL GETS
        CMP BUFFER.LEN, 0
        JE CALC
        XOR BX, BX
        CALL STR_TO_DATE
        JC FINALLY
    CATCH:
        PUTL ERR_MSG
        JMP TRY
    FINALLY:
        MOV DATE_ARRAY[ECX*2], BX
        INC ECX
    JMP LP
CALC:
    NWLN
    PUTL INDEX_INP_MSG
    INPINT INDEX_BUF 10
    MOV SI, DATE_ARRAY[EAX*2]
    INPINT INDEX_BUF 10
    MOV DI, DATE_ARRAY[EAX*2]
    CALL FIND_DATE_DIFF
    PUTL OUT_MSG
    MOV BX, 10
    CALL PUTINT
    NWLN
REP_CALC:
    PUTL REPEAT_MSG
    CALL GETCH
    OR AL, 20H      ; ������ � ������ ॣ����
    CMP AL, 'y'
    je CALC
    CMP AL, 'n'
    JE EXT
    PUTL ERR_MSG
    JMP REP_CALC
EXT:
    NWLN
    EXIT
    EXTRN	PUTSS:  NEAR
    EXTRN	PUTC:   NEAR
	EXTRN   SLEN:   NEAR
    EXTRN   GETS:   NEAR
    EXTRN   GETCH:  NEAR
    EXTRN   PUTINT: NEAR
    EXTRN   STR_TO_DATE:    NEAR
    EXTRN   DATE_TO_STR:    NEAR
    EXTRN   STR_TO_NUM:     NEAR
    EXTRN   FIND_DATE_DIFF: NEAR
END MAIN
