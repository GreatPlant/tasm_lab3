    MODEL SMALL

    include consts.inc
    include macros.mac
    .data
    .code
LOCALS
putss proc near
@@l:
    mov al,	[si]
    cmp al, 0ffh
    je @@r
    cmp al, 0
    jz @@e
    call putc
    inc si
    jmp	short @@l
    ; ����室 �� ᫥������ ��ப�
@@e:
    mov	al, CHCR
    call putc
    mov	al, CHLF
    call putc
@@r:
    ret
putss endp

putc proc near
    push ax
    push dx
    mov	dl, al
    mov	ah, FUPUTC
    int	DOSFU
    pop	dx
    pop ax
    ret
putc endp

getch proc near
    mov ah, FUGETCH
    int DOSFU
    ret
getch endp

gets proc near
    push ax
 	push si
 	mov si, dx
    mov	ah,	FUGETS
    int	DOSFU
 	; �ய����� ���� 0 � ����� ��ப�
 	xor	ah,	ah
 	mov	al,	[si+1]
 	add	si,	ax
 	mov	byte ptr [si+2], 0
 	pop	si
    pop ax
    NWLN
    ret
gets endp

slen proc near
	xor ax, ax
lslen:
    cmp byte ptr [si], 0
	je rslen
    cmp byte ptr [si], 0ffh
	je rslen
	inc ax
	inc si
	jmp short lslen
rslen:
    ret
slen endp

putint proc
	push 	ax bx cx
    xor 	cx,	cx
	test	ax, ax
  	jns 	@@next
	push	ax
	mov     al, '-'
	call	putc
	pop		ax
	neg		ax
@@next:
    xor	dx,	dx
    div	bx
    push dx
    inc	cx
    cmp	ax,	0
    jg	@@next
    mov ah, 2
;ᡮઠ ��᫠ � ����⭮� ���浪� � �뢮�
@@print:
    pop	dx
    add dl, '0'     ;�८�ࠧ������ ����� � ᨬ��� �����
    int 21h
    dec cx
    cmp cx, 0
    jg @@print
    mov dl, ' '
    int 21h
    pop cx bx ax
    ret
putint endp

STR_TO_NUM proc
LOCAL LP, ERROR, EXT
    push bx dx
    mov bx, 10
    xor dh, dh
    xor ax, ax
@@LP:
    cmp BYTE PTR [si], 0
    je @@EXT
        call BOOTHS_MUL_16
        mov dl, [si]   ; �८�ࠧ㥬 ᫥���騩 ᨬ��� � ��᫮
        cmp dl, '0'
        jb @@ERROR
        cmp dl, '9'
        ja @@ERROR
        sub dl, 30h
        add ax, dx
        jc @@ERROR    ; �᫨ �㬬� ������ 65535
        inc si
    JMP @@LP
@@EXT:
        pop dx bx
        clc
        ret
@@ERROR:
    xor di, di
    pop dx bx
    stc
    ret
STR_TO_NUM endp


; AX ��������
; BX �����⥫�
; AX १����
; BOOTHS_MUL_16 proc
;     push dx ;���筠� �㬬�
;     mov cx, 16
;     clc         ;b[-1] �� 㬮�砭�� 0
; @@lp:             ;�஢��塞 ����� �뫮 b[i-1]
;     jnc jif0    ;�᫨ �뫮 ࠢ�� 0, � ��룠�� �� j0
;     ror bx, 1   ;�஢��塞 �������樨 c 1
;     jnc jout     ;01 - ��諨 �� 楯� ������
;     jmp next    ;11 - ���室�� � ᫥���饬� 蠣�
; jif0:             ;�஢�ઠ �������権 00 10
;     ror bx, 1   ;�஢�ઠ 00 ��� 10?
;     jnc next    ;00 - ���室�� � ᫥���饬� 蠣�
;     sub dx, ax  ;10 - ��諨 � 楯� ������
;     jmp next
; jout:
;     add dx, ax
; next:
;     shl bx, 1
;     loop @@lp
;     mov ax, dx
;     pop dx
; BOOTHS_MUL_16 endp


BOOTHS_MUL_16 proc
    push cx dx
    mov dx, ax
    xor ax, ax
    mov cx, 16
    clc
@@lp:
    jnc a1
    ror dx, 1
    jnc a2
    sub ax, bx
    jmp a2
a1:
    ror dx, 1
    jnc a2
    add ax, bx
a2:
    shl ax, 1
    loop @@lp
    pop dx cx
BOOTHS_MUL_16 endp

PUBLIC PUTSS, PUTC, GETCH, GETS, PUTINT, STR_TO_NUM
end
